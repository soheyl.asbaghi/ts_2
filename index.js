// TODO: 1.Generics-->
function logData(data) {
    return data;
}
var outPut = logData('data'); /* logData<sting>('data') */
console.log(outPut);
var myFunc;
myFunc = function (data) {
    return data;
};
// __Generic Classes__
var MyArray = /** @class */ (function () {
    function MyArray(data) {
        this.data = data;
    }
    MyArray.prototype.addItem = function (item) {
        this.data.push(item);
    };
    MyArray.prototype.getItem = function (index) {
        return this.data[index];
    };
    return MyArray;
}());
var list = new MyArray([2, 3, 4, 5, 6, 7]);
console.log(list);
var Car = /** @class */ (function () {
    function Car(make, model, year) {
        this.make = make;
        this.model = model;
        this.year = year;
    }
    return Car;
}());
var myCar = new Car('Ford', 'Mustang', 1969);
console.log(myCar.model);
// TODO: 2.Typeof Type Operator -->
console.log(typeof 'Im Batman');
console.log(typeof 141);
console.log(typeof true);
function sum(x, y) {
    return x + y;
}
// ?darkMode: boolean;
// ?newUserProfile: boolean;
// !__Remove readonly__
// type CreateMutable<Type> = {
//   -readonly [Property in keyof Type]: Type[Property];
// };
// !__Remove optional__
// type Concrete<Type> = {
//   [Property in keyof Type]-?: Type[Property];
// };
// !__Remapping__
// type MappedTypeWithNewProperties<Type> = {
// [Properties in keyof Type as NewKeyType]: Type[Properties]
// };
