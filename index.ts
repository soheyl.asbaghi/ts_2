// TODO: 1.Generics-->

function logData<Type>(data: Type): Type {
  return data;
}

let outPut = logData('data'); /* logData<sting>('data') */
console.log(outPut);

interface myFuncInterface {
  <Type>(value: Type): Type;
}

let myFunc: myFuncInterface;
myFunc = function <Type>(data: Type): Type {
  return data;
};

// __Generic Classes__

class MyArray<Type extends number | string> {
  constructor(public data: Type[]) {}

  addItem(item: Type) {
    this.data.push(item);
  }

  getItem(index: number) {
    return this.data[index];
  }
}

let list = new MyArray<number>([2, 3, 4, 5, 6, 7]);
console.log(list);

interface Num {
  year: number;
}

class Car<S, N extends number> {
  constructor(public make: S, public model: S, public year: N) {}
}
let myCar = new Car<string, number>('Ford', 'Mustang', 1969);
console.log(myCar.model);

// TODO: 2.Keyof Type Operator -->

type Point = {
  x: number;
  y: number;
};

type P = keyof Point;

// TODO: 2.Typeof Type Operator -->

console.log(typeof 'Im Batman');
console.log(typeof 141);
console.log(typeof true);

type Filter = (filter: string) => boolean;
type Exist = ReturnType<Filter>;

function sum(x: number, y: number) {
  return x + y;
}
type S = ReturnType<typeof sum>;

// TODO: 3.Indexed Access Type -->

type Product = { title: string; price: number; exist: boolean };
type Title = Product['title'];
// ?type Title = string
type Book = Product['title' | 'price'];
// ?type Book = string | number
type NoteBook = Product[keyof Product];
// ?type NoteBook = string | number | boolean

// TODO: 4.Conditional Types -->
// !SomeType extends OtherType ? TrueType : FalseType;
interface Animal {
  live(): void;
}
interface Dog extends Animal {
  woof(): void;
}

type Example1 = Dog extends Animal ? number : string;
// ?type Example1 = number
type Example2 = RegExp extends Animal ? number : string;
// ?type Example2 = string

// TODO: 5.Mapped type

// define mapped type
type MapType<MT> = {
  [Property in keyof MT]: boolean;
};

type RegularType = {
  darkMode: () => void;
  newUserProfile: () => void;
};

type MapCopyRegular = MapType<RegularType>;
// ?darkMode: boolean;
// ?newUserProfile: boolean;

// !__Remove readonly__
// type CreateMutable<Type> = {
//   -readonly [Property in keyof Type]: Type[Property];
// };

// !__Remove optional__
// type Concrete<Type> = {
//   [Property in keyof Type]-?: Type[Property];
// };

// !__Remapping__
// type MappedTypeWithNewProperties<Type> = {
// [Properties in keyof Type as NewKeyType]: Type[Properties]
// };
